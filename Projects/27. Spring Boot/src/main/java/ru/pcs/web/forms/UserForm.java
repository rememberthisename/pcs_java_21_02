package ru.pcs.web.forms;

import lombok.Data;

/**
 * 03.12.2021
 * 27. Spring Boot
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class UserForm {
    private String firstName;
    private String lastName;
}
